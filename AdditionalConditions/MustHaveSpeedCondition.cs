﻿using UnityEngine;
using System.Collections;

namespace PlatformerPro
{
    /// <summary>
    /// COndition which is true if player has required X speed.
    /// </summary>
    public class MustHaveSpeedCondition : AdditionalCondition
    {

        public float requiredSpeed;

        public bool usePreviousFrame;
        
        /// <summary>
        /// Returns true if jump is being pressed.
        /// </summary>
        /// <returns>true</returns>
        /// <c>false</c>
        /// <param name="character">Character.</param>
        /// <param name="other">Other.</param>
        override public bool CheckCondition(Character character, object other)
        { 
            if (usePreviousFrame && Mathf.Abs(character.PreviousVelocity.x) >= requiredSpeed) return true;
            if (!usePreviousFrame && Mathf.Abs(character.Velocity.x) >= requiredSpeed) return true;
            return false;
        }
	
    }
}