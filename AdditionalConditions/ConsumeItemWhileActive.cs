﻿using UnityEngine;

namespace PlatformerPro.Extras
{
    /// <summary>
    /// A component that extends the must have item component to also consume the item while the given movement is active. 
    /// </summary>
    [RequireComponent(typeof(Movement))]
    public class ConsumeItemWhileActive : MustHaveItemCondition
    {
        [Header ("Ongoing Consumption - Energy")]
        [Tooltip("Item that is consumed")]
        [ItemType] [SerializeField] protected string itemToConsume;
        [Tooltip("How many items do we consume per second")]
        [SerializeField] protected float itemsPerSecond = 1.0f;
        protected Character character;
        protected Movement movement;

        protected float pendingConsumption;
        
        void Start()
        {
            character = GetComponentInParent<Character>();
            if (character == null)
            {
                Debug.LogWarning("ConsumeItemWhileActive could not find a character");
            }
            movement = GetComponent<Movement>();
        }
        
        void Update()
        {
            if (!TimeManager.SafeInstance.Paused && character.ActiveMovement == movement)
            {
                pendingConsumption += TimeManager.FrameTime;
                int amountToConsume = (int) pendingConsumption;
                if (amountToConsume >= 1)
                {
                    character.ItemManager.ConsumeItem(itemToConsume,amountToConsume);
                    pendingConsumption -= amountToConsume;
                }
            }
        }
    }
}