﻿using UnityEngine;
using System.Collections;

namespace PlatformerPro
{
    /// <summary>
    /// Condition which is true if player has required X and Y velocity.
    /// </summary>
    public class MustHaveVelocityCondition : AdditionalCondition
    {

        public Vector2 minVelocity;
        public Vector2 maxVelocity;

	public bool ignoreX;
	public bool ignoreY;

        public bool usePreviousFrame;
        
        /// <summary>
        /// Returns true if player has required velocity.
        /// </summary>
        /// <returns>true</returns>
        /// <c>false</c>
        /// <param name="character">Character.</param>
        /// <param name="other">Other.</param>
        override public bool CheckCondition(Character character, object other)
        { 
            if (usePreviousFrame && 
	        (ignoreX || (character.PreviousVelocity.x >= minVelocity.x && character.PreviousVelocity.x <= maxVelocity.x)) && 
	        (ignoreY || (character.PreviousVelocity.y >= minVelocity.y && character.PreviousVelocity.y <= maxVelocity.y))) return true; 
            if (!usePreviousFrame && 
	        (ignoreX || (character.Velocity.x >= minVelocity.x && character.Velocity.x <= maxVelocity.x)) && 
	        (ignoreY || (character.Velocity.y >= minVelocity.y && character.Velocity.y <= maxVelocity.y))) return true; 
            return false;
        }
    }
}
