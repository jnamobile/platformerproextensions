﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Journey.Geometry
{
   public class CharacterZPositionLimiter : MonoBehaviour
   {
      public float minZ;
      public float maxZ;

      public float requiredHeight = -1.0f;
      public bool minIsBlocker = true;
      public bool maxIsBlocker = true;
      
   }
}