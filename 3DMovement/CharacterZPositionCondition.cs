﻿using Journey.Player;
using PlatformerPro;
using UnityEngine;

namespace Journey.Geometry
{
    public class CharacterZPositionCondition : AdditionalCondition
    {
        public float minZ;

        public float maxZ;

        private CharacterZMover zMover;
        
        override public bool CheckCondition(Character character, object other)
        {
            if (character.transform.position.z < maxZ && character.transform.position.z > minZ) return true;
            return false;
        }

    }
}