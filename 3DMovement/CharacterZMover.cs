﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Journey.Geometry;
using UnityEngine;
using PlatformerPro;
using Debug = UnityEngine.Debug;

namespace Journey.Player
{
    public class CharacterZMover : MonoBehaviour
    {

        public float acceleration = 4.0f;
        public float maxSpeed = 4.0f;
        public float thresholdForMove = 2.0f;
        
        public float minZ;
        public float maxZ;
        
        public float VelocityZ { get; protected set; }
        
        private RaycastHit2D[] hits;
        
        private ContactFilter2D filter = new ContactFilter2D();

        private CharacterZPositionLimiter limiter;
        
        private Character character;
        
        private float MaxZ {
            get
            {
                if (limiter != null && limiter.maxIsBlocker) return limiter.maxZ;
                return maxZ;
            }
        }
        
        private float MinZ {
            get
            {
                if (limiter != null && limiter.minIsBlocker) return limiter.minZ;
                return minZ;
            }
        }
        
        // Start is called before the first frame update
        void Start()
        {
            PlatformerProGameManager.Instance.CharacterLoaded += InstanceOnCharacterLoaded;
            hits = new RaycastHit2D[4];
        }

        private void InstanceOnCharacterLoaded(object sender, CharacterEventArgs e)
        {
            character = e.Character;
        }

        // Update is called once per frame
        void Update()
        {
            if (character == null || TimeManager.Instance.Paused) return;
            DoUpdate();
        }

        void DoUpdate()
        {
            ProcessInput();
            CheckForLimits();
            MoveCharacter();
        }

        void ProcessInput()
        {
            if (character.Input.VerticalAxisDigital == 1)
            {
                VelocityZ += TimeManager.FrameTime * acceleration;
                if (VelocityZ > maxSpeed) VelocityZ = maxSpeed;
            } else if (character.Input.VerticalAxisDigital == -1)
            {
                VelocityZ += TimeManager.FrameTime * -acceleration;
                if (VelocityZ < -maxSpeed) VelocityZ = -maxSpeed;
            }
            else
            {
                if (VelocityZ > 0)
                {
                    VelocityZ += TimeManager.FrameTime * -acceleration;
                    if (VelocityZ < 0) VelocityZ = 0;
                } else if (VelocityZ < 0)
                {
                    VelocityZ += TimeManager.FrameTime * acceleration;
                    if (VelocityZ > 0) VelocityZ = 0;
                }
            }
        }

        void CheckForLimits()
        {
            Physics2D.Raycast((Vector2)transform.position + (10.0f * Vector2.up), Vector2.down, filter, hits, 100.0f);
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].collider != null)
                {
                    limiter = hits[i].collider.gameObject.GetComponent<CharacterZPositionLimiter>();

                    if (limiter != null)
                    {
                        if (transform.position.z <= limiter.maxZ && transform.position.z >= limiter.minZ && transform.position.y >= limiter.requiredHeight) break;
                        limiter = null;
                    }
                }
            }
        }
        
        void MoveCharacter()
        {
            if (!character.IsAttacking)
            {
                if (Mathf.Abs(VelocityZ) > thresholdForMove)
                {
                    character.transform.Translate(0, 0, VelocityZ * TimeManager.FrameTime, Space.World);
                }
            }

            if (character.transform.position.z >= MaxZ)
            {
                character.transform.position = new Vector3 (character.transform.position.x, character.transform.position.y, MaxZ);
                if (VelocityZ > 0)
                {
                    VelocityZ += TimeManager.FrameTime * 4 * -acceleration;
                    if (VelocityZ < 0) VelocityZ = 0;
                }
            }
            else if (character.transform.position.z <= MinZ)
            {
                character.transform.position = new Vector3 (character.transform.position.x, character.transform.position.y, MinZ);
                if (VelocityZ < 0)
                {
                    VelocityZ += TimeManager.FrameTime * 4 * acceleration;
                    if (VelocityZ > 0) VelocityZ = 0;
                }
            }
        }
    }
}