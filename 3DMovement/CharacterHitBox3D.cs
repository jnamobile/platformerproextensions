using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlatformerPro;

namespace Journey.Player
{
	/// <summary>
	/// The damage causing collider of a character or enemy, collides with hurt boxes to cause damage. This version uses 3D colliders instead of 2D ones.
	/// </summary>
	public class CharacterHitBox3D: CharacterHitBox
	{

		protected Collider myCollider3D;
		
		/// <summary>
		/// Gets the header string used to describe the component.
		/// </summary>
		/// <value>The header.</value>
		override public string Header
		{
			get
			{
				return "The damage causing collider of a character or enemy, collides with hurt boxes to cause damage. This version uses 3D colliders instead of 2D ones.";
			}
		}
		
		/// <summary>
		/// Init this instance, this should be called by the attack system during Start();
		/// </summary>
		override public void Init(DamageInfo info)
		{
			character = (IMob) gameObject.GetComponentInParent (typeof(IMob));
			myCollider3D = GetComponent<Collider>();
			if (myCollider3D == null)
			{
				Debug.LogError("A CharacterHitBox3D must be on the same GameObject as a Collider");
			}

			DamageType damageType = info.DamageType;
			if (!string.IsNullOrEmpty(weaponSlot))
			{
				ItemInstanceData weapon = Character.EquipmentManager.GetItemForSlot(weaponSlot);
				if (weapon != null && weapon.Data != null) damageType = weapon.Data.damageType;
			}
			damageInfo = new DamageInfo (info.Amount, damageType, Vector2.zero, character);
		}
		
		/// <summary>
		/// Start the hit with no timer.
		/// </summary>
		override public void Enable() 
		{
			hasHitCharacter = false;
			myCollider3D.enabled = true;
		}

		/// <summary>
		/// Start the hit.
		/// </summary>
		override public void Enable(float enableTime, float disableTime)
		{
			// Disable then restart
			myCollider3D.enabled = false;
			StopAllCoroutines();
			StartCoroutine(DoEnable (enableTime, disableTime));
		}

		/// <summary>
		/// Forces the attack to finish.
		/// </summary>
		override public void ForceStop()
		{
			StopAllCoroutines();
			myCollider3D.enabled = false;
			hitTimer = 0.0f;
		}

		/// <summary>
		/// Turn on the hit box.
		/// </summary>.
		/// <returns>The enable.</returns>
		/// <param name="enableTime">Enable time.</param>
		/// <param name="disableTime">Disable time.</param>
		override protected IEnumerator DoEnable(float enableTime, float disableTime)
		{
			hasHitCharacter = false;
			hitTimer = 0.0f;
			// Handle the timing, we don't use WaitForSeconds as we want to align with the internal frame time
			while (hitTimer < enableTime)
			{
				hitTimer += TimeManager.FrameTime;
				yield return true;
			}
			myCollider3D.enabled = true;
			while (hitTimer < disableTime)
			{
				hitTimer += TimeManager.FrameTime;
				yield return true;
			}
			myCollider3D.enabled = false;
		}

		/// <summary>
		/// Unity 2D trigger hook.
		/// </summary>
		/// <param name="other">Other.</param>
		void OnTriggerEnter(Collider other)
		{
			DoHit3D(other);
		}

		/// <summary>
		/// Do the actual hit.
		/// </summary>
		/// <param name="other">Other.</param>
		/// <returns>true if a hit was done.</returns>
		virtual protected bool DoHit3D(Collider other)
		{
			IHurtable hurtBox = (IHurtable) other.gameObject.GetComponent(typeof(IHurtable));
			// Got a hurt box and its not ourselves
			if (character != null && hurtBox != null && !hasHitCharacter && hurtBox.Mob != character )
			{
				damageInfo.Direction = transform.position - other.transform.position;
				damageInfo.DamageCauser = character;
				hurtBox.Damage(damageInfo);
				if (character is Character) ((Character)character).HitEnemy(hurtBox.Mob, damageInfo);
				hasHitCharacter = true;
				return true;
			}
			return false;
		}
	}
}