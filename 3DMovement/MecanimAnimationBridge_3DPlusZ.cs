using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Journey.Player;
using PlatformerPro;
using AnimationState = PlatformerPro.AnimationState;

namespace Journey.Animation
{
	/// <summary>
	/// An animator that sends sets state and previous state on a mecanim controller. Typically used for 3D models.
	/// </summary>
	public class MecanimAnimationBridge_3DPlusZ : MecanimAnimationBridge_3DExtraParams
	{
		[SerializeField] protected CharacterZMover characterZMover;

		override protected void Init()
		{
			base.Init();
			if (characterZMover == null && myCharacter != null && myCharacter is Component c)
			{
				characterZMover = c.gameObject.GetComponentInChildren<CharacterZMover>();
				
			}
			if (characterZMover == null)
			{
				Debug.LogWarning("No character Z mover found, disabling animation bridge");
				enabled = false;
			}
		}

		/// <summary>
		/// Implements the update code so it can be reused.
		/// </summary>
		override protected void ActualUpdate()
		{
			base.ActualUpdate();
			// Set velocity vars
			myAnimator.SetFloat("SpeedX", Mathf.Abs(myCharacter.Velocity.x));
			myAnimator.SetFloat("VelocityZ", characterZMover.VelocityZ);
		}

		/// <summary>
		/// Handles animation state changed.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="args">Arguments.</param>
		override protected void AnimationStateChanged(object sender, AnimationEventArgs args)
		{
			base.AnimationStateChanged(sender, args);
			// Set velocity vars
			myAnimator.SetFloat("SpeedX", Mathf.Abs(myCharacter.Velocity.x));
			myAnimator.SetFloat("VelocityZ", characterZMover.VelocityZ);
		}
	}
}

