﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using PlatformerPro;

namespace PlatformerPro.Extras
{
	/// <summary>
	/// Air movement which allows flying with a direct correlation between input and movement.
	/// </summary>
	public class AirMovement_FlyDirect : AirMovement
	{
		
		#region members
		
		/// <summary>
		/// The max speed the character moves in the air.
		/// </summary>
		[Header("Settings")]
		public float maxAirSpeed;

		/// <summary>
		/// Do we change speed based on analog input or do we also have speed 0/maxAirSpeed.
		/// </summary>
		public bool digital;

		#endregion
		
		#region constants
		
		/// <summary>
		/// Human readable name.
		/// </summary>
		private const string Name = "Special/Fly (Direct)";
		
		/// <summary>
		/// Human readable description.
		/// </summary>
		private const string Description = "Air movement which allows you to fly with a direct correlation between input and movement.";
		
		/// <summary>
		/// Static movement info used by the editor.
		/// </summary>
		new public static MovementInfo Info
		{
			get
			{
				return new MovementInfo(Name, Description, true);
			}
		}
		
		#endregion
		
		#region properties
		
		/// <summary>
		/// This class will handle gravity internally.
		/// </summary>
		override public bool ShouldApplyGravity
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this movement wants to control the movement in the air.
		/// Default is false with movement falling back to default air. Override if you want control.
		/// </summary>
		/// <value>true</value>
		/// <c>false</c>
		/// <returns><c>true</c>, if air control was wantsed, <c>false</c> otherwise.</returns>
		override public bool WantsAirControl()
		{
			if (!enabled) return false;
			return true;
		}

		#endregion
		
		#region public methods
		
		/// <summary>
		/// Gets a value indicating whether this movement wants to intiate the jump.
		/// </summary>
		/// <value><c>true</c> if this instance should jump; otherwise, <c>false</c>.</value>
		override public bool WantsJump()
		{
			if (!enabled) return false;
			// Check input and items
			if (character.Input.VerticalAxisDigital > 0 && character.Grounded)  return true;
			return false;
		}
		
		/// <summary>
		/// Moves the character.
		/// </summary>
		override public void DoMove()
		{
			Vector2 speedFactor = Vector2.zero;
			if (digital)
			{
				if (character.Input.HorizontalAxisDigital != 0)
				{
					speedFactor.x = character.Input.HorizontalAxisDigital == 1 ? 1.0f : -1.0f;
				}
				if (character.Input.VerticalAxisDigital != 0)
				{
					speedFactor.y = character.Input.VerticalAxisDigital == 1 ? 1.0f : -1.0f;
				}
				speedFactor.Normalize();
			}
			else
			{
				speedFactor = new Vector2(character.Input.HorizontalAxis, character.Input.VerticalAxis);
				speedFactor.Normalize();
				// Allow for less than full speed movement if the input isn't pushed all the way to the edges
				speedFactor *= Mathf.Max(Mathf.Abs(character.Input.HorizontalAxis), Mathf.Abs(character.Input.VerticalAxis));
			}
			Vector2 actualSpeed = maxAirSpeed * speedFactor;
			character.SetVelocityX(actualSpeed.x);
			character.SetVelocityY(actualSpeed.y);
			
			Debug.Log(actualSpeed);
			// Translate
			character.Translate(character.Velocity.x * TimeManager.FrameTime, character.Velocity.y * TimeManager.FrameTime, true);
		}
		
		/// <summary>
		/// Initialise the movement with the given movement data.
		/// </summary>
		/// <param name="character">Character.</param>
		/// <param name="movementData">Movement data.</param>
		override public Movement Init(Character character, MovementVariable[] movementData)
		{
			AssignReferences (character);
			return this;
		}
		
		/// <summary>
		/// If the jump just started force control.
		/// </summary>
		override public bool ForceMaintainControl()
		{
			if (!enabled) return false;
			// If on the ground but up being pressed keep control
			if (character.Input.VerticalAxisDigital > 0 && character.Grounded) return true;
			return false;
		}

		/// <summary>
		/// Gets the animation state that this movement wants to set.
		/// </summary>
		override public AnimationState AnimationState
		{
			get 
			{
				return AnimationState.FLY;
			}
		}
		
		/// <summary>
		/// Gets the priority for the animation state.
		/// </summary>
		override public int AnimationPriority
		{
			get 
			{
				return 0;
			}
		}
		
		/// <summary>
		/// Returns the direction the character is facing. 0 for none, 1 for right, -1 for left.
		/// This overriden version always returns the input direction.
		/// </summary>
		override public int FacingDirection
		{
			get 
			{
				return character.Input.HorizontalAxisDigital;
			}
		}
		
		#endregion
		
		#region protected methods
		
		/// <summary>
		/// Does the X movement.
		/// </summary>
		override protected void MoveInX (float horizontalAxis, int horizontalAxisDigital, ButtonState runButton)
		{
			// No need to do anything
		}

		/// <summary>
		///  Do the jump by translating and applying velocity.
		/// </summary>
		override public void DoJump()
		{
			// No need to do anything
		}
		
		/// <summary>
		/// Do the jump with overriden height and jumpCount.
		/// </summary>
		override public void DoOverridenJump(float newHeight, int newJumpCount, bool skipPowerUps = false)
		{
			Debug.LogWarning("Fly movement does not support overriden jump");
		}

		#endregion
	}
	
}