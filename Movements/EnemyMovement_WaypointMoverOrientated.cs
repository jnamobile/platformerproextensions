﻿using System.Collections.Generic;
using UnityEngine;

namespace PlatformerPro
{
    /// <summary>
    /// An enemy movement that moves through a set of waypoints.
    /// </summary>
    public class EnemyMovement_WaypointMoverOrientated : EnemyMovement_WaypointMover
	{
        private enum Orientation
        {
            Up,
            Down
        }

        [SerializeField]
        private List<Orientation> wayPointsOrientations;

        #region constants

        /// <summary>
        /// Human readable name.
        /// </summary>
        private const string Name = "Waypoint mover with orientation";
		
		/// <summary>
		/// Human readable description.
		/// </summary>
		private const string Description = "An enemy movement that moves through a set of waypoints which defined vertical orientation.";
		
		/// <summary>
		/// Static movement info used by the editor.
		/// </summary>
		new public static MovementInfo Info
		{
			get
			{
				return new MovementInfo(Name, Description);
			}
		}

        #endregion

        private int currentFacingDirection;

        public override EnemyMovement Init(Enemy enemy)
        {
            for (int i = 0; i < this.wayPoints.Count; i++)
            {
                // set default orientation if no orientation was set
                if (this.wayPointsOrientations.Count <= i)
                    this.wayPointsOrientations.Add(Orientation.Up);
            }
            return base.Init(enemy);
        }

        /// <summary>
		/// Returns the direction the character is facing. 0 for none, 1 for right, -1 for left.
		/// </summary>
        public override int FacingDirection
        {
            get
            {
                if (this.currentWayPoint >= this.wayPoints.Count)
                    return 0;

                // check if moving up or down
                if (this.wayPoints[this.currentWayPoint].x == this.enemy.Transform.position.x)
                {
                    return this.currentFacingDirection;
                }

                this.currentFacingDirection = this.wayPoints[this.currentWayPoint].x > this.enemy.Transform.position.x
                    ? this.wayPointsOrientations[this.currentWayPoint] == Orientation.Up ? 1 : -1
                    : this.wayPointsOrientations[this.currentWayPoint] == Orientation.Up ? -1 : 1;

                return this.currentFacingDirection;
            }
        }
    }

}