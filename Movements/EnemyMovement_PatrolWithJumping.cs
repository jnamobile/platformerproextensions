#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace PlatformerPro
{
	/// <summary>
	/// An enemy movement that patrols back and forth either jumping randomly or jumping when an enemy is found.
	/// </summary>
	public class EnemyMovement_PatrolWithJumping : EnemyMovement_Patrol
	{
		#region members

		[Header ("Jump settings")]

		/// <summary>
		/// When do we jump?
		/// </summary>
		public EnemyPatrolWithJump_JumpMode jumpMode;

		/// <summary>
		/// Maximum height we can jump.
		/// </summary>
		public float maxJumpHeight = 2.0f;

		/// <summary>
		/// Minimum height we can jump.
		/// </summary>
		public float minJumpHeight = 1.0f;

		/// <summary>
		/// Maximum length we can jump.
		/// </summary>
		public float maxJumpLength = 2.0f;

		/// <summary>
		/// Minimum length we can jump.
		/// </summary>
		public float minJumpLength = 1.0f;

		/// <summary>
		/// Angle along which we sense for player.
		/// </summary>
		public Vector2 senseRange;

		/// <summary>
		/// Layers to check for obstacle and characters.
		/// </summary>
		[Tooltip ("Layers to check for obstacle and characters. The enemy will be be able to 'see through' anything not in this layer mask.")]
		public LayerMask sightLayers;

		/// <summary>
		/// True when a jump has started.
		/// </summary>
		protected bool jumpStart;

		/// <summary>
		/// Should we show the inital jump animation.
		/// </summary>
		protected bool showJumpStartedAnimation;

		/// <summary>
		/// Height the jump should reach.
		/// </summary>
		protected float expectedJumpPeak;

		/// <summary>
		/// Starting jump velocity.
		/// </summary>
		protected float initialVelocity;

		/// <summary>
		/// Stores the speed required to jump the reuqired difference.
		/// </summary>
		protected float originalSpeed;

		/// <summary>
		/// Are we juping.
		/// </summary>
		protected bool isAJump;

		#endregion

		#region constants
		
		/// <summary>
		/// Human readable name.
		/// </summary>
		private const string Name = "Patrol with Jumping";
		
		/// <summary>
		/// Human readable description.
		/// </summary>
		private const string Description = "An enemy movement that patrols back and forth either jumping randomly or jumping when an enemy is found.";

		/// <summary>
		/// Static movement info used by the editor.
		/// </summary>
		new public static MovementInfo Info
		{
			get
			{
				return new MovementInfo(Name, Description);
			}
		}

		#endregion

		#region properties

		/// <summary>
		/// Does the movement.
		/// </summary>
		/// <returns>true</returns>
		/// <c>false</c>
		override public bool DoMove()
		{
			// If we are puased dont move as this will create an infinite required velocity
			if (TimeManager.FrameTime == 0) return true;
			// Determine what we want to do next
			if (enemy.Grounded && !jumpStart) {
				switch (jumpMode) {
				case EnemyPatrolWithJump_JumpMode.ALWAYS:
					DoJump ();
					break;
				case EnemyPatrolWithJump_JumpMode.SENSE_CHARACTER:
					if (DoSense()) DoJump ();
					break;
				}
			}
			// If we left the ground move state to JUMPING
			if (jumpStart) 
			{
				// Jump has moved beyond start once we leave the ground OR if we have hit our head (i.e. velocity back to zero or less)
				if (!enemy.Grounded || enemy.Velocity.y <= 0.0f) jumpStart = false;
				showJumpStartedAnimation = true;
			}
			MoveInX();
			MoveInY();
			return true;
		}

		/// <summary>
		/// Do the X movement.
		/// </summary>
		virtual protected void MoveInX () 
		{

			base.DoMove ();
		}

		/// <summary>
		/// Do the Y movement.
		/// </summary>
		virtual protected void MoveInY ()
		{
			// Apply gravity
			if (!enemy.Grounded || enemy.Velocity.y > 0)
			{
				// Falling
				if (enemy.Velocity.y <= 0.0f || (expectedJumpPeak - enemy.transform.position.y <= 0))
				{

				}
				// Jumping
				else
				{
					// Keep recalculating velocity to account for floating point discrepancies, grounded discrepancies, etc
					float requiredVelocity = Mathf.Sqrt(-2 *  (expectedJumpPeak - enemy.transform.position.y) * enemy.Gravity);
					if ((requiredVelocity * TimeManager.FrameTime) < 0.001f)
					{
						// Ensure we move at 0.001f or else we might get stuck
						requiredVelocity = (0.001f / TimeManager.FrameTime);
					}
					enemy.SetVelocityY(requiredVelocity);
				}
				
			}
			// Translate
			if (enemy.Grounded) enemy.Translate(0, enemy.Velocity.y * TimeManager.FrameTime, true);
			if (isAJump && enemy.Grounded && enemy.Velocity.y <= 0) {
				isAJump = false;
				if (enemy.LastFacedDirection == 1 && originalSpeed >= 0) speed = originalSpeed;
				if (enemy.LastFacedDirection == 1 && originalSpeed < 0) speed = -originalSpeed;
				if (enemy.LastFacedDirection == -1 && originalSpeed >= 0) speed = -originalSpeed;
				if (enemy.LastFacedDirection == -1 && originalSpeed < 0) speed = originalSpeed;
			}
		}

		/// <summary>
		///  Do the jump by translating and applying velocity.
		/// </summary>
		virtual protected void DoJump()
		{
			// If we allow jump height to be changed using tagged properties we need to recalcualte this
			float jumpHeight = Random.Range (minJumpHeight, maxJumpHeight);
			initialVelocity = Mathf.Sqrt(-2.0f * enemy.Gravity * jumpHeight);
			jumpStart = true;
			expectedJumpPeak = enemy.transform.position.y + jumpHeight;
			enemy.SetVelocityY(initialVelocity);
			isAJump = true;
			float jumpTime = -2 * initialVelocity / enemy.Gravity;
			float actualDistance = Random.Range (minJumpLength, maxJumpLength);
			float requiredJumpSpeed = actualDistance / jumpTime;
			if (speed < 0) requiredJumpSpeed *= -1;
			speed = requiredJumpSpeed;
		}

		/// <summary>
		/// Gets the animation state that this movement wants to set.
		/// </summary>
		override public AnimationState AnimationState
		{
			get 
			{
				if (showJumpStartedAnimation)
				{
					showJumpStartedAnimation = false;
					return AnimationState.JUMP;
				}
				if (enemy.Velocity.y > 0) return AnimationState.AIRBORNE;
				if (!enemy.Grounded) return AnimationState.FALL;
				return AnimationState.WALK;
			}
		}
		
		#endregion


		#region public methods
		
		/// <summary>
		/// Initialise this movement and return a reference to the ready to use movement.
		/// </summary>
		override public EnemyMovement Init(Enemy enemy)
		{
			this.enemy = enemy;
			leftExtent = enemy.transform.position.x - leftOffset;
			rightExtent = enemy.transform.position.x + rightOffset;
			originalSpeed = speed;
			return this;
		}

		/// <summary>
		/// Called when the enemy hits the character.
		/// </summary>
		/// <param name="character">Character.</param>
		/// <param name="info">Damage info.</param>
		override public void HitCharacter(Character character, DamageInfo info)
		{
			if (bounceOnHit) speed *= -1;
		}

		/// <summary>
		/// Called by the enemy to switch (x) direction of the movement. Note that not all 
		/// movements need to support this, they may do nothing.
		/// </summary>
		override public void SwitchDirection()
		{
			speed *= -1;
		}

		virtual protected bool DoSense()
		{
			RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector3(senseRange.x * enemy.LastFacedDirection, senseRange.y, 0), senseRange.magnitude, sightLayers);
			if (hit.collider != null)
			{
				Character character = null;
				ICharacterReference characterRef = (ICharacterReference) hit.collider.gameObject.GetComponent(typeof(ICharacterReference));
				if (characterRef == null)
				{
					character = hit.collider.gameObject.GetComponent<Character>();
				} 
				else
				{
					character = characterRef.Character;
				}
				if (character != null)
				{
					return true;
				}
			}
			return false;
		}
		#endregion

#if UNITY_EDITOR

		/// <summary>
		/// Draw handles for showing extents.
		/// </summary>
		void OnDrawGizmos()
		{
			DrawGizmos();
		}

		/// <summary>
		/// Draw gizmos for showing extents.
		/// </summary>
		override public void DrawGizmos()
		{
			// These handles don't make sense once the game is playing as they would move
			if (!Application.isPlaying)
			{
				base.DrawGizmos();

				Gizmos.color = Color.green;
				Gizmos.DrawLine(transform.position,  transform.position + new Vector3(senseRange.x, senseRange.y, transform.position.z));
			}
		}
#endif

	}

	public enum EnemyPatrolWithJump_JumpMode 
	{
		ALWAYS,
		SENSE_CHARACTER
	}

}