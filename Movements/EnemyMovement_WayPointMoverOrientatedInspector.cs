﻿using UnityEditor;

namespace PlatformerPro
{
    /// <summary>
    /// Inspector for characters classes.
    /// </summary>
    [CustomEditor(typeof(EnemyMovement_WaypointMoverOrientated), false)]
	public class EnemyMovement_WayPointMoverOrientatedInspector : EnemyMovement_WaypointMoverInspector
	{
	}
}
