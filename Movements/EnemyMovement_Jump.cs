#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace PlatformerPro
{
	/// <summary>
	/// An enemy movement that leaps in direction of player.
	/// </summary>
	public class EnemyMovement_Jump : EnemyMovement
	{

		/// <summary>
		/// How far we leap.
		/// </summary>
		[Tooltip ("How fast to leap.")]
		public float leapSpeed = 5.0f;

		/// <summary>
		/// The leap velocity.
		/// </summary>
		[Tooltip ("How much velocity to apply in y on the leap")]
		public float leapVelocity = 5.0f;

        /// <summary>
        /// The animation state to set while charging.
        /// </summary>
        [Tooltip("The animation state to set while charging.")]
        public AnimationState animationState = AnimationState.JUMP;

        /// <summary>
        /// Direction we are leaping.
        /// </summary>
        protected int leapDirection;

		/// <summary>
		/// Have we left ground?
		/// </summary>
		protected bool hasLeftGround;

		/// <summary>
		/// Tracks if leap started.
		/// </summary>
		protected bool leapStarted;

		/// <summary>
		/// Has leap finished?
		/// </summary>
		protected bool leapFinished;

		#region constants

		/// <summary>
		/// Human readable name.
		/// </summary>
		private const string Name = "Leap at Target";

		/// <summary>
		/// Human readable description.
		/// </summary>
		private const string Description = "An enemy movement that leaps in direction of player.";

		/// <summary>
		/// Static movement info used by the editor.
		/// </summary>
		new public static MovementInfo Info
		{
			get
			{
				return new MovementInfo(Name, Description);
			}
		}

        #endregion

        #region properties

        /// <summary>
        /// Gets the animation state that this movement wants to set.
        /// </summary>
        override public AnimationState AnimationState
        {
            get
            {
                return animationState;
            }
        }

        /// <summary>
        /// Returns the direction the character is facing. 0 for none, 1 for right, -1 for left.
        /// </summary>
        override public int FacingDirection
		{
			get 
			{
				return  leapDirection;
			}
		}

		#endregion



		#region public methods

		/// <summary>
		/// Initialise this movement and return a reference to the ready to use movement.
		/// </summary>
		override public EnemyMovement Init(Enemy enemy)
		{
			this.enemy = enemy;
			return this;
		}

		/// <summary>
		/// Moves the character.
		/// </summary>
		override public bool DoMove()
		{
			if (leapFinished) return false;
			if (!leapStarted)
			{
				enemy.SetVelocityY (leapVelocity);
				leapStarted = true;
				hasLeftGround = false;
				leapDirection = enemy.LastFacedDirection; // (enemy.CurrentTargetTransform.position.x < enemy.transform.position.x) ? -1 : 1;
			}
			else
			{
				if (hasLeftGround) 
				{
					if (enemy.Grounded)
					{
						leapFinished = true;
						enemy.MovementComplete();
					}
				} 
				else 
				{
					// Until we have left ground translate in y
					enemy.Translate(0, enemy.Velocity.y * TimeManager.FrameTime, true);
					if (!enemy.Grounded) hasLeftGround = true;
				}
			}

			enemy.ApplyGravity ();
			if (hasLeftGround)
			{
				enemy.SetVelocityX(leapSpeed * (float)leapDirection);
				enemy.Translate(leapSpeed * (float)leapDirection * TimeManager.FrameTime, 0, true);

			}
			return true;
		}

		/// <summary>
		/// Called when the enemy hits the character.
		/// </summary>
		/// <param name="character">Character.</param>
		/// <param name="info">Damage info.</param>
		override public void HitCharacter(Character character, DamageInfo info)
		{

		}

		/// <summary>
		/// Called by the enemy to switch (x) direction of the movement. Note that not all 
		/// movements need to support this, they may do nothing.
		/// </summary>
		override public void SwitchDirection()
		{

		}

		/// <summary>
		/// If we are grounded shoudl we snap to the ground. Helps us handle slopes.
		/// </summary>
		/// <value><c>true</c> if should snap to ground; otherwise, <c>false</c>.</value>
		override public bool ShouldSnapToGround
		{
			get 
			{
				return false;
			}
		}

		/// <summary>
		/// Called when this movement is gaining control.
		/// </summary>
		override public void GainingControl()
		{
			leapDirection = 0;
			hasLeftGround = false;
			leapStarted = false;
			leapFinished = false;
			enemy.continueMovementOnFall = true;
		}

		/// <summary>
		/// Called when this movement is losing control.
		/// </summary>
		override public bool LosingControl()
		{
			leapDirection = 0;
			hasLeftGround = false;
			leapStarted = false;
			leapFinished = false;
			enemy.continueMovementOnFall = false;
			return false;
		}

	/*	/// <summary>
		/// Gets the animation state that this movement wants to set.
		/// </summary>
		override public AnimationState AnimationState
		{
			get 
			{
				return AnimationState.JUMP;
			}
		}
        */
		#endregion

		#region protected methods

		/// <summary>
		/// Set the direction of the charge.
		/// </summary>
		/// <param name="direction">Direction.</param>
		override public void SetDirection(Vector2 direction)
		{
			Debug.LogWarning ("EnemyMovement_LeapAtPlayer doesn't support SetDirection . It always runs at target");
		}

		#endregion

		#if UNITY_EDITOR

		// Place holder for draw gizmos, etc.
		#endif

	}

}